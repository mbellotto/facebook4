# facebook4

## Tutorial Facebook login

El código fuente se puede encontrar en la secciónde download del proyecto [https://bitbucket.org/mbellotto/facebook4/downloads/fblogin.zip]()

### Paso 1. Crear app Ionic 

``` bash
# Ionic V3
ionic start myApp blank --type=angular

# Ionic V4
ionic start myApp blank
```

### Paso 2. Crear aplicación en Facebook Apps

Desde [https://developers.facebook.com/apps]() crear la aplicación. Seguir las instrucciones. Al terinar de crear la aplicación se debe conservar el ID de la aplicación (si estas en español aparece como "IDENTIFICADOR DE LA APP: 516033822299999") y el nombre que se le dio a la aplicación al momento de crearla. Ambos datos son necesarios para configurar el plugin y pueden ser ingresados al momento de instalar o posteriormente editando los archivos de configuración del proyecto.

### Paso 3. Instalar plugin Facebook4 desde Ionic Native

``` bash
# en este caso se intala el plugin con los datos de la aplicación en Facebook
ionic cordova plugin add cordova-plugin-facebook4 --save --variable APP_ID="516033822299999" --variable APP_NAME="edental"
```

-----
### NOTA:

Si no se ha suministrado los datos de la aplicación de facebook al instalar el plugin

``` bash
# en este caso se intala el plugin con los datos de la aplicación en Facebook
ionic cordova plugin add cordova-plugin-facebook4 --save 
```

Entonces se puede editar el archivo **package.json** (que se encuentra en el raíz del proyecto) en la sección "cordova" y "plugins" introduciendo las líneas que se resaltan en el siguiente código

```
    ...
    "cordova": {
        "plugins": {
            "cordova-plugin-facebook4": {
                "APP_ID": "516033822259443",       //  Línea a agregar
                "APP_NAME": "edental"              //  Línea a agregar 
            },
            "cordova-plugin-device": {},
            "cordova-plugin-ionic-keyboard": {},
            "cordova-plugin-ionic-webview": {},
            "cordova-plugin-splashscreen": {},
            "cordova-plugin-statusbar": {},
            "cordova-plugin-whitelist": {}
        },
        "platforms": [
            "browser"
        ]
    },
    ...
```

Además, se debe editar el archivo **config.xml** como se indica en el siguiente fragmento de código. Se debe buscar el pugar donde se encuentra la declaración del plugin facebook4 y agregar las dos variables

``` XML
    <plugin name="cordova-plugin-facebook4" spec="^4.2.1">
        <variable name="APP_ID" value="516033822259443" /> 
        <variable name="APP_NAME" value="edental" />
    </plugin>
```

### Paso 4. Instalar el servicio Ionic Native que permite hacer uso del Plugin

``` bash
npm install @ionic-native/facebook
```

### Paso 5. Agregar el servicio a la aplicación

en el módulo **app.module.ts** agregar el servicio como un provider

``` javascript
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Importar el servicio
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    Facebook,           // Agregar el servicio como un provider
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

### Paso 5. Hacer uso del servicio

**home.page.ts**
``` javascript
import { Component } from '@angular/core';

// Importar nuevamente el servicio
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    constructor(
        private fb: Facebook   // Inyectar el servicio para su uso en el componente
    ) { }


    login() {
        // Llamar al servicio
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
            .catch(e => console.log('Error logging into Facebook', e));
    }

}
```


``` html
<ion-header>
  <ion-toolbar>
    <ion-title>
      Ionic Blank
    </ion-title>
  </ion-toolbar>
</ion-header>

<ion-content padding>
  The world is your oyster.
  <p>If you get lost, the <a target="_blank" rel="noopener" href="https://ionicframework.com/docs/">docs</a> will be your guide.</p>

  <ion-button (click)="login()">
    Login with Facebook
  </ion-button>
</ion-content>

```

### Paso 6. Usar el servicio desde un Browser (OPCIONAL)

El plugin permite ser usado desde el browser, pero para ello cordova debe estar funcionando, Esto solo sirve con propositos de debug y testing.

``` bash
# Instalar la plataforma correspondiente para el browser
ionic cordova platform add browser

#llamar a la aplicación mediante cordova
cordova serve

# Opcionalmente también puede ser convocado mediante
ionic cordova run browser
```

### Links relacionados

**Documentación del Plugin en Ionic Native**

[https://ionicframework.com/docs/native/facebook]()

**Documentación y código del Plugin en GitHub**

[https://github.com/jeduan/cordova-plugin-facebook4]()

**Sitio para el registro de aplicaciones en Facebook**

[https://developers.facebook.com/apps]()

**Tutorial alternativo**

[https://ionicthemes.com/tutorials/about/ionic-facebook-login]()

**Código fuente del proyecto Ioniv V4**

[https://bitbucket.org/mbellotto/facebook4/downloads/fblogin.zip]()